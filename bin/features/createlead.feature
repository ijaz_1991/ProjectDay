#Feature: CreateLead in leaftap Application
#Scenario: Positive flow for Create Lead
#Given launch the browser
#And maximize the browser
#And set the timeouts
#And enter the URL
#And enter the username as DemoSalesManager
#And enter the password as crmsfa
#When click on the login button
#And click on CRMSFA link
#And click on Create Lead link
#And enter the company name as cognizant
#And enter the first name as mohamed
#And enter the second name as sachin
#When click on the Create Lead button
#Then verify Create Lead is success
Feature: CreateLead in leaftap Application

Background:
Given launch the browser
And maximize the browser
And set the timeouts
And enter the URL

Scenario: positive flow for createLead
And enter the username as DemoSalesManager
And enter the password as crmsfa
And click on the login button
And click on CRMSFA link
And click on Create Lead link
And enter the company name as cts
And enter the first name as mohamed
And enter the second name as sachin
When click on the Create Lead button
Then verify Create Lead is success

Scenario Outline: negative flow for creatLead
And enter the username as <username>
And enter the password as <password>
And click on the login button
And click on CRMSFA link
And click on Create Lead link
And enter the company name as cts
And enter the first name as mohamed
And enter the second name as sachin
When click on the Create Lead button
Then verify Create Lead is success

Examples:
|username|password|
|DemoSalesManager|crmsfa|
|DemoSalesManager|crmsfa|

#Scenario Outline: Negative flow for Create Lead 
#And enter the username as <username>
#And enter the password as <password>
#When click on the login button
#And click on CRMSFA link
#And click on Create Lead link
#And enter the company name as cognizant
#And enter the first name as mohamed
#And enter the second name as sachin
#When click on the Create Lead button
#But verify Create Lead is success
#
#Examples:
#|username|password|
#|DemoSalManage|crmsfa|
#|Demo|Cfa|
#
#
