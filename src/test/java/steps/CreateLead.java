/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	ChromeDriver driver;	
	
	@Given("launch the browser")
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}
	@And("maximize the browser")
	public void maximize()
	{
		driver.manage().window().maximize();
	}
	@And("set the timeouts")
	public void setTimeouts() {
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
	}
	@And("enter the URL")
	public void loadurl()
	{
		driver.get("http://leaftaps.com/opentaps/control/main");
	}
	
	@And("enter the username as (.*)")
	public void enterusername(String Uname)
	{
		driver.findElementById("username").sendKeys(Uname);
	}
	@And("enter the password as (.*)")
	public void enterpassword(String Pwd)
	{
		driver.findElementById("password").sendKeys(Pwd);
	}
	@When("click on the login button")
	public void clicklogin()
	{
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@And("click on CRMSFA link")
	public void clickcrmsfa()
	{
		driver.findElementByLinkText("CRM/SFA").click();
	}
	@And("click on Create Lead link")
	public void clickcreatelead()
	{
		driver.findElementByLinkText("Create Lead").click();
	}
	@And("enter the company name as(.*)")
	public void entercompanyname(String Cname)
	{
		driver.findElementById("createLeadForm_companyName").sendKeys(Cname);
	}
	@And("enter the first name as(.*)")
	public void enterfirstname(String Fname)
	{
		driver.findElementById("createLeadForm_firstName").sendKeys(Fname);
	}
	@And("enter the second name as (.*)")
	public void enterlastname(String Lname)
	{
		driver.findElementById("createLeadForm_lastName").sendKeys(Lname);
	}
	@When("click on the Create Lead button")
	public void clickcreateleadbutton()
	{
		driver.findElementByClassName("smallSubmit").click();
	}
	@Then("verify Create Lead is success")
	public void verify()
	{
		System.out.println("success");
	}

}
*/