package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import BankPages.BankBazaarFirstPage;
import BankPages.SearchMutual;
import wdMethods.ProjectMethods;
import wdMethods.bankPM;

public class BankBazzarTestCase extends bankPM {

	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "BB";
		testCaseDescription ="TT";
		category = "Smoke";
		author= "Babu";
		//dataSheetName="TC001";
	}
	@Test//(dataProvider="fetchData")
	public  void basic()   {
		new BankBazaarFirstPage()
		.mouseOverOnInvestment()
		.selectMutualFund()
		.SearchFunds()
		.dob()
		.dateofbirth()
		.dayofbirth()
		.selcontinue()
		.selsalary()
		.selcont()
		.selectbankaccount()
		.enterfname()
		.viewmutual()
		.alertcancel()
		.getSchemes();

		//.SelectMutualFund()


	}


}
