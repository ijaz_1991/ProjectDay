package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MyHomePage extends ProjectMethods{

	
	//public MyLeadsPage clickLeads()
	public MyLeadsPage clickLeads()
	{
		WebElement eleLeads = locateElement("linktext", "Leads");
		click(eleLeads);
		return new MyLeadsPage();
		
	}
	public MyFindLeadPage findleads()
	{
		WebElement eleFindLeads = locateElement("linktext", "Find Leads");
		click(eleFindLeads);
		return new MyFindLeadPage();
	}
	
	public CreateLeadPage clickCreateLead()
	{
		WebElement eleLeads = locateElement("linktext", "Leads");
		click(eleLeads);
		//return new MyLeadsPage();
		return new CreateLeadPage();
	}
}









