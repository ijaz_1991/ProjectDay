package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MyFindLeadPage extends ProjectMethods{

	
	public MyFindLeadPage typeLeadphnum(String data){
		WebElement elephonenumber = locateElement("name", "phoneNumber");
		type(elephonenumber, data);
		return this;

	}
	public MyFindLeadPage clickfindleadButton()
	{
		WebElement elefindlead = locateElement("xpath", "//button[text()='Find Leads']");
		click(elefindlead);
		return this;
	}
	public MyViewLeadPage clickfirstlead() {
		WebElement elefirstrecord =locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(elefirstrecord);
		return new MyViewLeadPage();
	}
	public CreateLeadPage clickCreateLead() {
		// TODO Auto-generated method stub
		return new CreateLeadPage();
	}
}









