package wdMethods;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import utils.DataInputProvider;

public class bankPM extends SeMethods {
	
	public String dataSheetName;
	//@Parameters({"url","uname","pwd"})
	@BeforeMethod
	public void login() {
		startApp("chrome", "https://www.bankbazaar.com");
		
	}
	
	//@AfterMethod(groups="common")
	@AfterMethod
	public void close() {
		closeAllBrowsers();
	}
	
	@DataProvider(name="fetchData")
	public String[][] getData() {
	return DataInputProvider.getSheet(dataSheetName);
	}
	

}
