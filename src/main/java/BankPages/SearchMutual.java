package BankPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.bankPM;

public class SearchMutual extends bankPM {
	public SearchMutual SearchFunds() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement Search = locateElement("linktext","Search for Mutual Funds");
		click(Search);
		return this;
	}
	public SearchMutual dob() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String requiredAge="27";
		WebElement drag = locateElement("xpath","//div[@class='rangeslider__handle']");
		WebElement age = locateElement("xpath","//div[@class='rangeslider__handle']/div");
		String text=age.getText();
		Actions builder=new Actions(driver);
		do {
			builder.dragAndDropBy(drag, 5, 0).build().perform();
			age=locateElement("xpath","//div[@class='rangeslider__handle']/div");
			text=age.getText();
		}while(!text.equals(requiredAge));
		return this;
		}
	public SearchMutual dateofbirth() {
		WebElement dateofbir = locateElement("linktext","Jul 1991");
		click(dateofbir);
		return this;
	}
	public SearchMutual dayofbirth() {
		WebElement selday = locateElement("xpath","//div[@aria-label='day-9']");
		click(selday);
		return this;
	}
	public SearchMutual selcontinue() {
		WebElement seleccont = locateElement("linktext","Continue");
		click(seleccont);
		return this;
	}
	public SearchMutual selsalary() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	String requiredSalary="4,25,000";
	WebElement drag = locateElement("xpath","//div[@class='rangeslider__handle']");
	WebElement salary = locateElement("xpath","//div[@class='rangeslider__handle-label']/span");
	String text =salary.getText();
	Actions builder = new Actions(driver);
	do {
		builder.dragAndDropBy(drag, 5, 0).build().perform();
		salary= locateElement("xpath","//div[@class='rangeslider__handle-label']/span");
		text=salary.getText();
	} while (!text.equals(requiredSalary));
	return this;
	}
	public SearchMutual selcont() {
		WebElement continu = locateElement("linktext","Continue");
		click(continu);
		return this;
	}
	public SearchMutual selectbankaccount() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement selbank = locateElement("xpath","(//input[@name='primaryBankAccount'])[2]");
		click(selbank);
		return this;
	}
	public SearchMutual enterfname() {
		locateElement("xpath","//input[@name='firstName']").sendKeys("ijaz");
		return this;
		
	}
	public BankBazaarMutualPage viewmutual() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement viewmutualbutton = locateElement("linktext","View Mutual Funds");
		click(viewmutualbutton);
		return new BankBazaarMutualPage();
	}
	
	
}

