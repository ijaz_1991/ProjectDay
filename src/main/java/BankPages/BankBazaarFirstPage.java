package BankPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import wdMethods.bankPM;

public class BankBazaarFirstPage extends bankPM {
	public BankBazaarFirstPage mouseOverOnInvestment() {
		WebElement SelcInvest = locateElement("linktext","INVESTMENTS");
		Actions action=new Actions(driver);
		action.moveToElement(SelcInvest).build().perform();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//WebElement SelcMutual = locateElement("linktext","Mutual Funds");
		//click(SelcMutual);
		return this;
	}
	
	public SearchMutual selectMutualFund() {
		 WebElement SelMutual = locateElement("linktext","Mutual Funds");
		  click(SelMutual);
		  try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new SearchMutual();
	}
	
	
	
}


